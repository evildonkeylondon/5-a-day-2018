<?php date_default_timezone_set('Europe/London'); ?>
<?php
// process.php

$errors         = array();      // array to hold validation errors
$data           = array();      // array to pass back data

// validate the variables ======================================================
    // if any of these variables don't exist, add an error to our $errors array
    function filterEmail($field){
        // Sanitize e-mail address
        $field = filter_var(trim($field), FILTER_SANITIZE_EMAIL);
        
        // Validate e-mail address
        if(filter_var($field, FILTER_VALIDATE_EMAIL)){
            return $field;
        }else{
            return FALSE;
        }
    }

    // Functions to filter user inputs
    function filterName($field){
        // Sanitize user name
        $field = filter_var(trim($field), FILTER_SANITIZE_STRING);
        
        // Validate user name
        if(filter_var($field, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+/")))){
            return $field;
        }else{
            return FALSE;
        }
    }

    if (empty($_POST['email'])) {
        $errors['email'] = 'Email is required.';
    } else{
        $email = filterEmail($_POST["email"]);
        if($email == FALSE){
            $errors['email'] = 'Please enter a valid email address.';
        }
    }

    // Validate user name
    if(empty($_POST["firstname"])){
        $errors['firstname'] = 'Your name is required.';
    }else{
        $name = filterName($_POST["firstname"]);
        if($name == FALSE){
            $errors['firstname'] = 'Please enter a valid frist name.';
        }
    }
    // Validate user surname
    if(empty($_POST["lastname"])){
        $errors['lastname'] = 'Your surname is required.';
    }else{
        $surname = filterName($_POST["lastname"]);
        if($surname == FALSE){
            $errors['lastname'] = 'Please enter a valid last name.';
        }
    }

    $enquiry = $_POST["enquiry"];

// return a response ===========================================================

    // if there are any errors in our errors array, return a success boolean of false
    if ( ! empty($errors)) {

        // if there are items in our errors array, return those errors
        $data['success'] = false;
        $data['errors']  = $errors;
    } else {

        // if there are no errors process our form, then return a message

        // DO ALL YOUR FORM PROCESSING HERE
        // THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)
        $to = 'info@5-a-dayfitness.com';
        
        $email_content .= "From: $name $surname\n\n";
        $email_content .= "Email: $email\n\n";
        $email_content .= "Message: $enquiry\n\n";

        // Build the email headers.
        $email_headers = "From: <$email>";
        
        $subject = "5 a day contact form";

        // Sending email
        if(mail($to, $subject, $email_content, $email_headers)){
            // show a message of success and provide a true success variable
            $data['success'] = true;
            $data['message'] = 'Your email has been submitted.';
        } else {
            $data['success'] = false;
            $data['errors']  = $errors;
        }
    }

    // return all our data to an AJAX call
    echo json_encode($data);