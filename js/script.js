$(document).ready(function(){
	
	//fade-in logo
	$(".hero-image").hide(0).delay(200).fadeIn(2000);

	//Nav Icon

	$('#nav-icon').click(function(){
		$(this).toggleClass('open');
	});

	//Slick
	$('.slider').slick({
	 slidesToShow: 1,
		dots: true,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000,
		arrows: false,
    accessibility: false
	  });

	//AOS
	AOS.init();

	//CTA Buttons fade in
	$(document).scroll(function () {
    var y = $(this).scrollTop();
    if (y > 590) {
        $('.bottomMenu').fadeIn();
    } else {
        $('.bottomMenu').fadeOut();
    }
  });

$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 1000, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});


(function( $ ) {
    // the sameHeight functions makes all the selected elements of the same height
    $.fn.sameHeight = function() {
        var selector = this;
        var heights = [];

        // Save the heights of every element into an array
        selector.each(function(){
            var height = $(this).height();
            heights.push(height);
        });

        // Get the biggest height
        var maxHeight = Math.max.apply(null, heights);
        // Show in the console to verify
        console.log(heights,maxHeight);

        // Set the maxHeight to every selected element
        selector.each(function(){
            $(this).height(maxHeight);
        }); 
    };
 
}( jQuery ));

$('.box').sameHeight();

$(window).resize(function(){
    // Do it when the window resizes too
    $('.box').sameHeight();
});
		
});








$(document).ready(function() {

    // process the form
    $('form').submit(function(event) {

        $('.help-block').remove(); // remove the error text

        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'firstname' : $('input[name=firstname]').val(),
            'lastname' : $('input[name=lastname]').val(),
            'email' : $('input[name=email]').val(),
            'enquiry' : $('textarea[name=enquiry]').val()
        };

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'process.php', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
            // using the done promise callback
            .done(function(data) {

                // log data to the console so we can see
                console.log(data); 

                // here we will handle errors and validation messages
                if ( ! data.success) {

                    // handle errors for email ---------------
                    if (data.errors.email) {
                        console.log('here');
                        $('form').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                    }
                    if (data.errors.firstname) {
                        console.log('here 2');
                        $('form').append('<div class="help-block">' + data.errors.firstname + '</div>'); // add the actual error message under our input
                    }
                    if (data.errors.lastname) {
                        console.log('here 3');
                        $('form').append('<div class="help-block">' + data.errors.lastname + '</div>'); // add the actual error message under our input
                    }

                } else {

                    console.log('success');

                    // ALL GOOD! just show the success message!
                    // $('.submitted-form').slideUp();
                    $('input, textarea').val('');
                    $('form').append('<div class="help-block help-success">' + data.message + '</div>');

                    // usually after form submission, you'll want to redirect
                    // window.location = '/thank-you'; // redirect a user to another page

                }
            })
            // using the fail promise callback
            // .fail(function(data) {
            .fail(function( data) {

                // show any errors
                // best to remove for production
                // console.log(data);
                console.log('fail');
            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

});
